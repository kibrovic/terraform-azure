resource "azurerm_kubernetes_cluster" "k8s" {
  location            = azurerm_resource_group.rg.location
  name                = "aks-${var.cluster_name}"
  resource_group_name = azurerm_resource_group.rg.name
  node_resource_group = "${azurerm_resource_group.rg.name}-aks"
  dns_prefix          = var.cluster_name
  tags = {
    Environment = "Development"
  }

  default_node_pool {
    name       = "default"
    vm_size    = "Standard_B2s"
    node_count = "1"
  }
  linux_profile {
    admin_username = "ubuntu"

    ssh_key {
      key_data = file(var.ssh_public_key)
    }
  }
  network_profile {
    network_plugin    = "kubenet"
    load_balancer_sku = "standard"
  }
  service_principal {
    client_id     = var.aks_service_principal_client_id
    client_secret = var.aks_service_principal_client_secret
  }
}

resource "azurerm_kubernetes_cluster_node_pool" "development" {
  name                  = "development"
  kubernetes_cluster_id = azurerm_kubernetes_cluster.k8s.id
  vm_size               = var.dev_vm_size
  enable_auto_scaling   = true
  min_count             = 0
  max_count             = var.dev_agent_count

  node_labels = {
    env = "development"
  }

  tags = {
    Environment = "development"
  }
}

resource "azurerm_kubernetes_cluster_node_pool" "production" {
  name                  = "production"
  kubernetes_cluster_id = azurerm_kubernetes_cluster.k8s.id
  vm_size               = var.prod_vm_size
  enable_auto_scaling   = true
  min_count             = 0
  max_count             = var.prod_agent_count

  node_labels = {
    env = "production"
  }

  node_taints = [
    "env=production:NoSchedule"
  ]


  tags = {
    Environment = "production"
  }
}
