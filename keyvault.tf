resource "azurerm_key_vault" "client" {
  name                = "kv-${var.cluster_name}"
  location            = var.resource_group_location
  resource_group_name = azurerm_resource_group.rg.name
  sku_name            = "standard"
  tenant_id           = data.azurerm_client_config.current.tenant_id

  #   lifecycle {
  #     prevent_destroy = true
  #   }
}

resource "azurerm_key_vault_access_policy" "devops" {
  key_vault_id = azurerm_key_vault.client.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azuread_group.devops.object_id

  key_permissions = []

  secret_permissions = [
    "Backup",
    "Delete",
    "Get",
    "List",
    "Purge",
    "Recover",
    "Restore",
    "Set",
  ]

  storage_permissions = []
}

resource "azurerm_key_vault_access_policy" "developers" {
  key_vault_id = azurerm_key_vault.client.id
  tenant_id    = data.azurerm_client_config.current.tenant_id
  object_id    = azuread_group.developers.object_id

  key_permissions = []

  secret_permissions = [
    "Get",
    "List",
    "Set",
  ]

  storage_permissions = []
}