resource "azuread_group" "devops" {
  display_name     = "DevOps"
  security_enabled = true
}

resource "azuread_group" "developers" {
  display_name     = "Developers"
  security_enabled = true
}