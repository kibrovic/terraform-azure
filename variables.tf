variable "dev_vm_size" {
  default = "Standard_B2ms"
}

variable "dev_agent_count" {
  default = 1
}

variable "prod_vm_size" {
  default = "Standard_B2ms"
}

variable "prod_agent_count" {
  default = 1
}

# The following two variable declarations are placeholder references.
# Set the values for these variable in terraform.tfvars
variable "aks_service_principal_client_id" {
  default = ""
}

variable "aks_service_principal_client_secret" {
  default = ""
}

variable "cluster_name" {
  default = "private"
}

variable "dns_name" {
  default = "private.test.com"
}

variable "resource_group_location" {
  default     = "eastus"
  description = "Location of the resource group."
}

variable "ssh_public_key" {
  default = "~/.ssh/id_rsa.pub"
}
