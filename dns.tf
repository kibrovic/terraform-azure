resource "azurerm_dns_zone" "dns" {
  name                = var.dns_name
  resource_group_name = azurerm_resource_group.rg.name
}